package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Persona")

public class Persona {

	private long id;
	private String nombre;
    private String primer_apellido;
    private String segundo_apellido;
    private String telefono;
    private String status;
    
    public Persona() {
    	  
    }
 
    public Persona(String nombre, String primer_apellido, String segundo_apellido,String telefono,String status) {
         this.nombre = nombre;
         this.primer_apellido = primer_apellido;
         this.segundo_apellido = segundo_apellido;
         this.telefono = telefono;
         this.status = status;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
        public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
 
    @Column(name = "nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Column(name = "primer_apellido", nullable = false, length = 100)
    public String getprimer_apellido() {
        return primer_apellido;
    }
    public void setprimer_apellido(String primer_apellido) {
        this.primer_apellido = primer_apellido;
    }
    
    @Column(name = "segundo_apellido", nullable = false, length = 100)
    public String getsegundo_apellido() {
        return segundo_apellido;
    }
    
    public void setsegundo_apellido(String segundo_apellido) {
        this.segundo_apellido = segundo_apellido;
    }
    
    @Column(name = "telefono", nullable = false, length = 100)
    public String gettelefono() {
        return telefono;
    }
    
    public void settelefono(String telefono) {
        this.telefono = telefono;
    }
    
    @Column(name = "status", nullable = false, length = 1)
    public String getstatus() {
        return status;
    }
    public void setstatus(String status) {
        this.status = status;
    }
    
    
}
