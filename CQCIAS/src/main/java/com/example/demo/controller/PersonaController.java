package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import javax.validation.Valid;

import com.example.demo.exception.ResourceNotFoundException;
import com.example.demo.model.Persona;
import com.example.demo.repository.PersonaRepository;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class PersonaController {
    @Autowired
    private PersonaRepository personaRepository;

    @GetMapping("/personas")
    public List<Persona> getAllPersons() {
        return personaRepository.findAll();
    }

    @GetMapping("/personas/{id}")
    public ResponseEntity<Persona> getEmployeeById(@PathVariable(value = "id") Long personaId)
        throws ResourceNotFoundException {
    	Persona persona = personaRepository.findById(personaId)
          .orElseThrow(() -> new ResourceNotFoundException("Employee not found for this id :: " + personaId));
        return ResponseEntity.ok().body(persona);
    }
    
    @PostMapping("/personas")
    public Persona createEmployee(@Valid @RequestBody Persona employee) {
        return personaRepository.save(employee);
    }

    
}